#!/usr/bin/python
# -*-coding: utf-8 -*
from FacebookWebBotUpdate import *
from datetime import datetime
from run import *
import logging
import time
import sys
import re
import io

localtime = time.localtime(time.time())
DATE_SEPARATOR = 'DTSTART:'
NAME_SEPARATOR = 'SUMMARY:Anniversaire de '
CURRENT_MONTH = localtime.tm_mon
CURRENT_DAY = localtime.tm_mday
MAX_DAYS = 15 								#days before the Birthdate you want to send message to

BIRTHDAY_WISH = ""
IMAGE_TO_SEND = []
WEBDRIVER = "phantom"

"""
	Load the ICS file to parse.
	return list of tuple containing (birthdate, fb_name)
"""
def load_ICS(name, DS=DATE_SEPARATOR, NS=NAME_SEPARATOR):
	csv_name = name.replace("ics", "csv")
	fh = io.open(name, mode="r", encoding="utf-8")
	f = io.open(name, mode="r", encoding="utf-8")
	b = list(filter(lambda x: x.find(DATE_SEPARATOR) >= 0, fh))
	n = list(filter(lambda x: x.find(NAME_SEPARATOR) >= 0, f))
	sb = '(?<={}).*?(?=\n)'.format(DATE_SEPARATOR)
	sn = '(?<={}).*?(?=\n)'.format(NAME_SEPARATOR)
	b = [re.search(sb, x).group() for x in b]
	n = [re.search(sn, x).group() for x in n]
	data = map(lambda x,y:(x,y), b, n)
	logger.info("Loading data 	DONE")
	return data

"""
	Check if this is time to send a message or not
	This will return true if the birthdate is between todaY and MAX_DAYS days later.
	return  Boolean
"""
def is_birthday(friend, logger):
	#ics file adds + 1 year to the calendar, so we need to insert the current Year instead
	cur_date = datetime.strptime('{}{}{}'.format(localtime.tm_year, localtime.tm_mon, localtime.tm_mday), '%Y%m%d')
	logger.info(cur_date)
	f_date = datetime.strptime(friend[0], '%Y%m%d')
	logger.info(f_date)
	#check if the birthdate is within 'MAX_DAYS' days
	logger.info(f_date - cur_date)
	diff_days = int(str(f_date - cur_date)[:2])
	logger.info(diff_days)
	return True if (diff_days >= 0 and diff_days <= MAX_DAYS) else False

"""
	Refresh the time to compare the date after
	every cycles.
"""
def refresh_date(logger):
	localtime = time.localtime(time.time())
	CURRENT_MONTH = localtime.tm_mon
	CURRENT_DAY = localtime.tm_mday
	logger.info("Time Refreshed!")

"""
	Main loops, configure the bot
	and variables then execute the main loop.
"""
def birthday_chat(friends_data, logger):
	LOGIN_EMAIL, LOGIN_PWD = run()
	while 1:
		i = 0
		logger.info("Credentials 	[OK]")
		bot = FacebookBot(WEBDRIVER)
		bot.set_page_load_timeout(10)
		bot.login(LOGIN_EMAIL, LOGIN_PWD)
		for f in friends_data:
			print(f)
			if (is_birthday(f, logger)):
				i += 1
				logger.info("	Send Request to {}!".format(f[1]))
				bot.newMessageToFriend(f[1], BIRTHDAY_WISH, *IMAGE_TO_SEND)
				#Wait 3 sec , instead of rushing it.
				time.sleep(3)
		#parse done , sleep for 1 day now
		logger.info("\n	[{}] Friends has received your message!".format(i))
		logger.info("\nDisconnect The current session")
		bot.logout()
		logger.info("Sleep for a day")
		time.sleep(60 * 60 * 24)
		refresh_date(logger)
		

if __name__ == '__main__':
	logging.basicConfig(level=logging.INFO)
	logger = logging.getLogger("BOT")
	BIRTHDAY_WISH = check_args(sys.argv, logger)
	IMAGE_TO_SEND = get_imgages(sys.argv, logger)
	WEBDRIVER = get_webdriver(sys.argv, logger)
	data = load_ICS(*sys.argv[2:])
	birthday_chat(data, logger)