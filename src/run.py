import getpass
import sys
import os
import io

VERSION = "2.3.1"

def welcome():
	print(" _______ __     _______")
	print("|    ___|  |--.|   |   |.-----.-----.-----.-----.-----.-----.-----.----.")
	print("|    ___|  _  ||       ||  -__|__ --|__ --|  -__|     |  _  |  -__|   _|")
	print("|___|   |_____||__|_|__||_____|_____|_____|_____|__|__|___  |_____|__|")
	print("                                                      |_____|")
	print(" ______         __")
	print("|   __ \\.-----.|  |_")
	print("|   __ <|  _  ||   _|")
	print("|______/|_____||____|")
	print("Welcome to FbMessengerBot --> by Punkachu")
	print("VERISON: " + VERSION)

def usage(name):
	print("Missing Arguments...")
	print("USAGE: {} perso_msg_file DATA.ics phantom/chrome [image1.png[,image2.jpg, image3.gif]]".format(name, "phantom or chrome"))
	print("QUITTING...")


def check_args(args, logger):
	if (len(args) < 2):
		usage(args[0])
		sys.exit()
	logger.info("INPUT CHECKED 		[OK]")
	msg = ""
	try:
		f = io.open(args[1], mode="r", encoding="utf-8")
		msg = f.read()
		f.close()
	except:
		logger.error("INPUT CHECKED 		[KO]")
		logger.error("Can't open the file: {}".format(args[1]))
		sys.exit()
	return msg

def get_imgages(args, logger):
	if (len(args) < 4):
		logger.error("No pictures had been provided")
		return None
	logger.info("Found image path {}".format(args[4]))
	l = args[4].split(',')
	l = [ os.path.abspath(item) for item in l ]
	logger.info("Found image path {}".format(l))
	return l

def get_webdriver(args, logger):
	if (len(args) < 3):
		logger.error("No webdriver specified")
		usage(args[0])
		sys.exit()
	return args[3]

"""
	Get the username and the password
	The password will be protected by 
	the module getpass
"""
def run():
	email = input('Please Enter your email: ')
	pwd = getpass.getpass("Please Enter your password: ")
	print("Proceed Initialization...")
	return email, pwd
