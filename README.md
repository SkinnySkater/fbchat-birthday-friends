# FbChatBirthdayBot 
[![Python Versions][pyversion-button]]
[pyversion-button]: http://img.shields.io/pypi/pyversions/Markdown.svg

Python Wrapper for sending custom messages before birthday of your friends
Made by using the existing Python warper [FacebookWebBot](https://github.com/hikaruAi/FacebookBot).

__Disclaimer__: This wrapper is __NOT__ an official wrapper.

### About

The bot is using selenium and PhantomJs to drive as headless browser.
No API is required.

The program is mainly made for people who are lazy about sending birthday wishes everydays(yep some people got more than 2000 friends, probably at the very start of facebook ... :] )
Or for those who needs a tiny program to interact with large amount of people (friends here ...) and take advantage of the birthday to propose a potential service that they may need to celebrate their birthday.
In short: You may need it for marketing ! Or modify it to fit any crazy Idea!


### Usage

It reads **ICS** files, you need to provide in the command line the 2
tags that will be stored to extract the date and name lines inside the ICS file.


```bash
python bot.py perso_msg_file  file.ics phantom/chrome  [image1.png[,image2.jpg, image3.gif]]
```

You'll need to setup [Phantomjs](http://phantomjs.org/), then set up the PATH ENV in your os.
And download [Chrome Driver](https://sites.google.com/a/chromium.org/chromedriver/downloads), then copy/paste the .exe on windows dir in your C driver.
And set up  [Selenium](http://www.techbeamers.com/selenium-webdriver-python-tutorial/) to contact facebook through the browser.

### Installation

```bash
pip install pymessenger #OPTIONAL
pip install selenium
pip install io
```

